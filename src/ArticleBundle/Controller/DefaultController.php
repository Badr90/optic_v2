<?php

namespace ArticleBundle\Controller;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// Import new namespaces
use AppBundle\Entity\Contact;
use AppBundle\Entity\Article;
use AppBundle\Form\ContactType;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends Controller
{
    /**
     * @Route("/article/{slug}")
     */
    public function indexAction($slug)
    {
        $em = $this->getDoctrine()->getManager();

        $article = $em->getRepository("AppBundle:Article")->findOneBy(array('articleUrl' =>$slug));

        $nb =$article->getNombreVisites();

        //dump($nb);
        $article->setNombreVisites(++$nb);

        $em->persist($article);
        $em->flush();

        //dump($article->getNombreVisites());die();
        //var_dump($article->getMarque()->getNomMarque());
        $marqueId=$article->getMarque()->getId();

        $articlesSimilaires =$em->getRepository("AppBundle:Article")->findBy(array('marque' => $marqueId ),
        array('nombreVisites' => 'DESC'),
        4);//limit

        return $this->render('ArticleBundle:Default:index.html.twig', array(
            'article'=> $article
            ,'articlesSimilaires' => $articlesSimilaires
        ));
    }

    /**
     * @Route("/marque/{slug}")
     */
    public function showMarqueAction($slug, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $articles = $em->getRepository("AppBundle:Article")->getarticlesByMarqueAndViews($slug);


        $marque=$em->getRepository("AppBundle:Marque")->findOneBy(array('url' =>$slug));

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $articles, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            6/*limit per page*/
        );

        // set an array of custom parameters
        $pagination->setCustomParameters(array(
            'align' => 'center', # center|right (for template: twitter_bootstrap_v4_pagination)
        ));
        //dump($pagination);die();
        return $this->render('ArticleBundle::marque.html.twig', array(
            'articlemarques'=> $articles,
            'marque' =>$marque->getNomMarque(),
             array('pagination' => $pagination),
            'pagination' => $pagination
            ));
    }

    /**
     * @Route("/optique")
     */
    public function showOptiqueAction()
    {
        $em = $this->getDoctrine()->getManager();
        $articles = $em->getRepository("AppBundle:Article")->findBy(array('typeLunettes' =>'optique'));

        return $this->render('ArticleBundle::optique.html.twig', array(
            'articleoptiques'=> $articles
        ));
    }

    /**
     * @Route("/solaire")
     */
    public function showSolaireAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $articles = $em->getRepository("AppBundle:Article")->findBy(array('typeLunettes' =>'solaire'));

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $articles, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            6/*limit per page*/
        );

        // set an array of custom parameters
        $pagination->setCustomParameters(array(
            'align' => 'center', # center|right (for template: twitter_bootstrap_v4_pagination)
        ));

        return $this->render('ArticleBundle::solaire.html.twig', array(
            'articlesolaires'=> $articles,
            array('pagination' => $pagination),
            'pagination' => $pagination
        ));
    }
    /**
     * @Route("/enfant")
     */
    public function showEnfantAction()
    {
        $em = $this->getDoctrine()->getManager();
        $articles = $em->getRepository("AppBundle:Article")->findBy(array('typeLunettes' =>'enfant'));

        return $this->render('ArticleBundle::enfants.html.twig', array(
            'articlenfants'=> $articles
        ));
    }

    /**
     * @Route("/offres")
     */
    public function showOffresAction()
    {

        return $this->render('ArticleBundle::offres.html.twig'
       );
    }

    /**
     * @Route("/contact")
     */
    public function showContactAction(Request $request)
    {


        $enquiry = new Contact();

        $form =$this->createFormBuilder($enquiry)
            ->add('Nom', TextType::class

                )
            ->add('Prenom', TextType::class)
            ->add('Sujet', TextType::class)
            ->add('email', TextType::class)
            ->add('message', TextareaType::class)
            ->getForm();

        $request = $request;//->getRequest();
        if ($request->getMethod() == 'POST') {

            //envoi d'email

            $transport = \Swift_SmtpTransport::newInstance('localhost', 25);//('smtp.gmail.com', 465, 'ssl')
            $message = \Swift_Message::newInstance($transport)
                ->setSubject('test subject')
                ->setFrom('test@test.com')
                ->setTo(['badrassa2@gmail.com'])
                ->setBody(
                    'test'
                );
            $this->get('mailer')->send($message);

/*
 *          $mailer= new \Swift_Mailer();
            $message = (new \Swift_Message('Hello Email'))
                ->setFrom('send@example.com')
                ->setTo('badrassa2@gmail.com')
                ->setBody(
                    "MESSAGE TEST"
                );

            $mailer->send($message);
*/
            //mail('badrassa2@gmail.com', 'Mon Sujet', 'Mon Messages');

            return $this->render('ArticleBundle::emailBienEnvoye.html.twig');
           // }
        }
        return $this->render('ArticleBundle::contact.html.twig', array(
                'form' => $form->createView()
            )
        );
    }

    /**
     * @Route("/cart_process")
     */
    public function storeItemsAction(){
        $total_items=9;
return json_encode(array('items'=>$total_items));
        /*
        session_start();

        if(isset($_POST['total_cart_items']))
        {
        echo count($_SESSION['name']);
        exit();
        }

        if(isset($_POST['item_src']))
        {
            $_SESSION['name'][]=$_POST['item_name'];
            $_SESSION['price'][]=$_POST['item_price'];
            $_SESSION['src'][]=$_POST['item_src'];
            echo count($_SESSION['name']);
            exit();
        }

        if(isset($_POST['showcart']))
        {
            for($i=0;$i<count($_SESSION['src']);$i++)
            {
                echo "<div class='cart_items'>";
                echo "<img src='".$_SESSION['src'][$i]."'>";
                echo "<p>".$_SESSION['name'][$i]."</p>";
                echo "<p>".$_SESSION['price'][$i]."</p>";
                echo "</div>";
            }
            exit();
        }*/
        }
}

<?php

/**
 * Created by PhpStorm.
 * User: badr
 * Date: 6/8/17
 * Time: 11:08 PM
 */
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;

class ContactType extends AbstractType
{

    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder->add('name');
        $builder->add('email', 'email');
        $builder->add('subject');
        $builder->add('body', 'textarea');
    }

    public function getName()
    {
        return 'contact';
    }

}
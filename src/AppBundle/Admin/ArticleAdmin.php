<?php
namespace AppBundle\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Article;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Intl\NumberFormatter;
use Symfony\Polyfill\Util\Binary;


class ArticleAdmin extends AbstractAdmin
{
    protected $translationDomain = 'SonataPageBundle'; // default is 'messages'


    // add this method
  /*  public function validate(ErrorElement $errorElement, $object )
    {
        $errorElement
            //reference non null
            ->with('description')
            ->assertLength(['max' => 3])
            ->end()
        ;
    }
*/

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('imageGauche600X250')
            ->add('imageGauche800x350')
            ->add('imageDroite600X250')
            ->add('imageDroite800x350')
            ->add('imageProfil800x350')
            ->add('imageProfil600X250')
            ->add('typeLunettes')
            ->add('enStock')
            ->add('description')
            ->add('nombreVisites')
            ->add('dateCreation');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $object = $this->id($this->getSubject());

        $listMapper
            ->add('id')
            ->add('imageGauche600X250', 'file', array(
                'template' => "ArticleBundle:Default:image1_admin.html.twig", 'object' => $object))
            ->add('imageGauche800x350', 'file', array(
                'template' => "ArticleBundle:Default:image2_admin.html.twig", 'object' => $object))
            ->add('imageDroite600X250', 'file', array(
                'template' => "ArticleBundle:Default:image3_admin.html.twig", 'object' => $object))
            ->add('imageDroite800x350', 'file', array(
                'template' => "ArticleBundle:Default:image4_admin.html.twig", 'object' => $object))
            ->add('imageProfil600X250', 'file', array(
                'template' => "ArticleBundle:Default:image5_admin.html.twig", 'object' => $object))
            ->add('imageProfil800x350', 'file', array(
                'template' => "ArticleBundle:Default:image6_admin.html.twig", 'object' => $object))
            ->add('typeLunettes')
            ->add('enStock')
            ->add('description')
            ->add('nombreVisites')
            ->add('dateCreation')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ));
    }



    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

        $object = $this->id($this->getSubject());

        $subject = $this->getSubject();



        $formMapper
            ->add('id', 'integer', array('required' => false, 'attr' => array('readonly' => true)));

        //si l objet n existe pas : nouvelles images
        if ($subject->getId() == null) {

            $formMapper->add('imageGauche800x350', 'file', array('required' => true, 'data_class' => null))
                ->add('imageGauche600X250', 'file', array('required' => true, 'data_class' => null))
                ->add('imageDroite600X250', 'file', array('required' => false, 'data_class' => null))
                ->add('imageDroite800x350', 'file', array('required' => false, 'data_class' => null))
                ->add('imageProfil800x350', 'file', array('required' => false, 'data_class' => null))
                ->add('imageProfil600X250', 'file', array('required' => false, 'data_class' => null));

        } //sinon reprendre les photos :
        else{
        	$ImageGauche600X250 = $this->getSubject()->getImageGauche600X250path();
        	$ImageGauche800X350 = $this->getSubject()->getImageGauche800X350path();
        	 
        	$ImageDroite600X250 = $this->getSubject()->getImageDroite600X250path();
        	$ImageDroite800X350 = $this->getSubject()->getImageDroite800X350path();
        	 
        	$ImageProfil600X250 = $this->getSubject()->getImageProfil600X250path();
        	$ImageProfil800X350 = $this->getSubject()->getImageProfil800X350path();
        	 
        	// GAUCHE use $fileFieldOptions so we can add other options to the field
        	$fileFieldOptions1 = ['required' => false, 'data_class' => null];
        	if ($ImageGauche600X250 ) {
        		// get the container so the full path to the image can be set
        		$container = $this->getConfigurationPool()->getContainer();
        		$fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath().'/'.$ImageGauche600X250;
        		// add a 'help' option containing the preview's img tag
        		$fileFieldOptions1['help'] = '<img src="'.$fullPath.'" class="admin-preview" />';
        	}
        	
        	$fileFieldOptions2 = ['required' => false, 'data_class' => null];	 
        	if ($ImageGauche800X350 ) {
        		$container = $this->getConfigurationPool()->getContainer();
        		$fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath().'/'.$ImageGauche800X350;
        		$fileFieldOptions2['help'] = '<img src="'.$fullPath.'" class="admin-preview" />';
        	}
        	//droite
        	$fileFieldOptions3 = ['required' => false, 'data_class' => null];
        	if ($ImageDroite600X250 ) {
        		$container = $this->getConfigurationPool()->getContainer();
        		$fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath().'/'.$ImageDroite600X250;
        		$fileFieldOptions3['help'] = '<img src="'.$fullPath.'" class="admin-preview" />';
        	}
        	$fileFieldOptions4 = ['required' => false, 'data_class' => null];
        	 
        	if ($ImageDroite800X350 ) {
        		$container = $this->getConfigurationPool()->getContainer();
        		$fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath().'/'.$ImageDroite800X350;
        		$fileFieldOptions4['help'] = '<img src="'.$fullPath.'" class="admin-preview" />';
        	}
        	
        	//profil
        	$fileFieldOptions5 = ['required' => false, 'data_class' => null];
        	if ($ImageProfil600X250 ) {
        		$container = $this->getConfigurationPool()->getContainer();
        		$fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath().'/'.$ImageProfil600X250;
        		$fileFieldOptions5['help'] = '<img src="'.$fullPath.'" class="admin-preview" />';
        	}
        	$fileFieldOptions6 = ['required' => false, 'data_class' => null];
        	
        	if ($ImageProfil800X350 ) {
        		$container = $this->getConfigurationPool()->getContainer();
        		$fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath().'/'.$ImageProfil800X350;
        		$fileFieldOptions6['help'] = '<img src="'.$fullPath.'" class="admin-preview" />';
        	}
        $formMapper->add('imageGauche600X250', 'file', $fileFieldOptions1)
        
        //->add('imageGauche600X250path', 'text', array('attr'=>array("hidden" => true)))
        
        	->add('imageGauche800x350', 'file',$fileFieldOptions2)
          
            ->add('imageDroite600X250', 'file', $fileFieldOptions3)
            ->add('imageDroite800x350', 'file', $fileFieldOptions4)
            
            ->add('imageProfil800x350', 'file', $fileFieldOptions5)
            ->add('imageProfil600X250', 'file', $fileFieldOptions6)

            ->add('typeLunettes', 'choice', array('required' => true,
            'choices' => array('Optique' => 'optique', 'Solaire' => 'solaire', 'Enfant' => 'enfant')
        ))
            ->add('enStock')
            ->add('description', 'text',
                array('label' => 'Référence')
            )
            ->add('Details', 'text',
                array('required' => false, 'label' => 'Détails de l\'article')
            )
            ->add('marque', 'sonata_type_model_list',
                array('label' => 'Choisissez la marque'),
                array(), array(
                    'placeholder' => 'Marque'
                )
            );
       
        }
    }
    public function getNewInstance()
    {
        $instance = parent::getNewInstance();
        $instance->setImageGauche800X350($instance->getImageGauche800X350());

        return $instance;
    }
 

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $object= $this->id($this->getSubject());

        $showMapper
            ->add('id', 'integer', array('required' => false, 'attr' => array('readonly' => true)))

            ->add('imageGauche600X250', 'file', array(
            'template' => "ArticleBundle:Default:image1_admin.html.twig",'object'=>$object))
            ->add('imageGauche800x350', 'file', array(
                'template' => "ArticleBundle:Default:image2_admin.html.twig",'object'=>$object))

            ->add('imageDroite600X250', 'file', array(
        'template' => "ArticleBundle:Default:image3_admin.html.twig",'object'=>$object))
            ->add('imageDroite800x350', 'file', array(
                'template' => "ArticleBundle:Default:image4_admin.html.twig",'object'=>$object))

            ->add('imageProfil600x250', 'file', array(
        'template' => "ArticleBundle:Default:image5_admin.html.twig",'object'=>$object))
            ->add('imageProfil800X350', 'file', array(
                'template' => "ArticleBundle:Default:image6_admin.html.twig",'object'=>$object))
            ->add('typeLunettes')
            ->add('enStock')
            ->add('description')
            ->add('nombreVisites')
            ->add('dateCreation')
        ;
    }


}
?>

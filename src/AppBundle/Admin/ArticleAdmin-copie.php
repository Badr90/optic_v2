<?php
namespace AppBundle\Admin;


use AppBundle\Entity\Article;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Intl\NumberFormatter;
use Symfony\Polyfill\Util\Binary;


class ArticleAdmin extends AbstractAdmin
{
    protected $translationDomain = 'SonataPageBundle'; // default is 'messages'


    // add this method
  /*  public function validate(ErrorElement $errorElement, $object )
    {
        $errorElement
            //reference non null
            ->with('description')
            ->assertLength(['max' => 3])
            ->end()
        ;
    }
*/

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('imageGauche600X250')
            ->add('imageGauche800x350')
            ->add('imageDroite600X250')
            ->add('imageDroite800x350')
            ->add('imageProfil800x350')
            ->add('imageProfil600X250')
            ->add('typeLunettes')
            ->add('enStock')
            ->add('description')
            ->add('nombreVisites')
            ->add('dateCreation');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $object = $this->id($this->getSubject());

        $listMapper
            ->add('id')
            ->add('imageGauche600X250', 'file', array(
                'template' => "ArticleBundle:Default:image1_admin.html.twig", 'object' => $object))
            ->add('imageGauche800x350', 'file', array(
                'template' => "ArticleBundle:Default:image2_admin.html.twig", 'object' => $object))
            ->add('imageDroite600X250', 'file', array(
                'template' => "ArticleBundle:Default:image3_admin.html.twig", 'object' => $object))
            ->add('imageDroite800x350', 'file', array(
                'template' => "ArticleBundle:Default:image4_admin.html.twig", 'object' => $object))
            ->add('imageProfil600X250', 'file', array(
                'template' => "ArticleBundle:Default:image5_admin.html.twig", 'object' => $object))
            ->add('imageProfil800x350', 'file', array(
                'template' => "ArticleBundle:Default:image6_admin.html.twig", 'object' => $object))
            ->add('typeLunettes')
            ->add('enStock')
            ->add('description')
            ->add('nombreVisites')
            ->add('dateCreation')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ));
    }



    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

        $object = $this->id($this->getSubject());

        $subject = $this->getSubject();


        $formMapper
            ->add('id', 'integer', array('required' => false, 'attr' => array('readonly' => true)));

        //si l objet n existe pas : nouvelles images
        if ($subject->getId() == null) {

            $formMapper->add('imageGauche600X250', 'file', array('required' => true, 'data_class' => null))
                ->add('imageGauche800x350', 'file', array('required' => true, 'data_class' => null))
                ->add('imageDroite600X250', 'file', array('required' => false, 'data_class' => null))
                ->add('imageDroite800x350', 'file', array('required' => false, 'data_class' => null))
                ->add('imageProfil800x350', 'file', array('required' => false, 'data_class' => null))
                ->add('imageProfil600X250', 'file', array('required' => false, 'data_class' => null));

        } //sinon reprendre les photos :
        else {
/*
             if ($subject->getImageGauche800x350() != null) {
                $fileFieldOptions['help'] = '<img src="data:image/jpg;base64,' . $subject->getEncodedGaucheImage800x350() . '"  />';

                $formMapper->add('imageGauche800X350', 'file', array('required' => false, 'data_class' => null//,'delete' => false
                   ,// 'allow_delete' => false,

                ), $fileFieldOptions);
            }
                        if ($subject->getImageGauche600x250() != null) {
                            $fileFieldOptions['help'] = '<img src="data:image/jpg;base64,' . $subject->getEncodedGaucheImage600x250() . '"  />';
                            $formMapper->add('imageGauche600X250', 'file', array('required' => false, 'data_class' => null), $fileFieldOptions);
                        }

                                    if($subject->getImageDroite800x350()!=null) {
                                        $fileFieldOptions['help'] = '<img src="data:image/jpg;base64,' . $subject->getEncodedDroiteImage800x350() . '"  />';
                                        $formMapper->add('imageDroite800X350', 'file', array('required' => false,'data_class' => null), $fileFieldOptions);
                                    }
                                    if($subject->getImageDroite600x250()!=null) {
                                        $fileFieldOptions['help'] = '<img src="data:image/jpg;base64,' . $subject->getEncodedDroiteImage600x250() . '"  />';
                                        $formMapper->add('imageDroite600X250', 'file', array('required' => false,'data_class' => null), $fileFieldOptions);
                                    }
                                    if($subject->getImageProfil800x350()!=null) {
                                        $fileFieldOptions['help'] = '<img src="data:image/jpg;base64,' . $subject->getEncodedProfilImage800x350() . '"  />';
                                        $formMapper->add('imageProfil800X350', 'file', array('required' => false,'data_class' => null), $fileFieldOptions);
                                    }
                                    if($subject->getImageProfil600x250()!=null) {
                                        $fileFieldOptions['help'] = '<img src="data:image/jpg;base64,' . $subject->getEncodedProfilImage600x250() . '"  />';
                                        $formMapper->add('imageProfil600X250', 'file', array('required' => false,'data_class' => null), $fileFieldOptions);
                                    }*/
        }
        $formMapper->add('typeLunettes', 'choice', array('required' => true,
            'choices' => array('Optique' => 'optique', 'Solaire' => 'solaire', 'Enfant' => 'enfant')
        ))
            ->add('enStock')
            ->add('description', 'text',
                array('label' => 'Référence')
            )
            ->add('Details', 'text',
                array('required' => false, 'label' => 'Détails de l\'article')
            )
            ->add('marque', 'sonata_type_model_list',
                array('label' => 'Choisissez la marque'),
                array(), array(
                    'placeholder' => 'Marque'
                )
            );
        // ->add('nombreVisites')
        // ->add('dateCreation')
        ;
    }
    public function getNewInstance()
    {
        $instance = parent::getNewInstance();
        $instance->setImageGauche800X350($instance->getImageGauche800X350());

        return $instance;
    }
    /*
    public function prePersist($image)
    {  $image= $this->getSubject()->getImageGauche800x350();
        $this->manageFileUpload($image);
    }

    public function preUpdate($image)
    {
        $image= $this->getSubject()->getImageGauche800x350();
        $this->manageFileUpload($image);
    }

    private function manageFileUpload($image)
    {
        $image= $this->getSubject()->getImageGauche800x350();

        if ($image->getFile()) {
            $image->refreshUpdated();
        }
    }
*/

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $object= $this->id($this->getSubject());

        $showMapper
            ->add('id', 'integer', array('required' => false, 'attr' => array('readonly' => true)))

            ->add('imageGauche600X250', 'file', array(
            'template' => "ArticleBundle:Default:image1_admin.html.twig",'object'=>$object))
            ->add('imageGauche800x350', 'file', array(
                'template' => "ArticleBundle:Default:image2_admin.html.twig",'object'=>$object))

            ->add('imageDroite600X250', 'file', array(
        'template' => "ArticleBundle:Default:image3_admin.html.twig",'object'=>$object))
            ->add('imageDroite800x350', 'file', array(
                'template' => "ArticleBundle:Default:image4_admin.html.twig",'object'=>$object))

            ->add('imageProfil600x250', 'file', array(
        'template' => "ArticleBundle:Default:image5_admin.html.twig",'object'=>$object))
            ->add('imageProfil800X350', 'file', array(
                'template' => "ArticleBundle:Default:image6_admin.html.twig",'object'=>$object))
            ->add('typeLunettes')
            ->add('enStock')
            ->add('description')
            ->add('nombreVisites')
            ->add('dateCreation')
        ;
    }


}
?>

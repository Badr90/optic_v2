<?php
/**
 * Created by PhpStorm.
 * User: badr
 * Date: 7/26/17
 * Time: 9:36 PM
 */

namespace AppBundle\Admin;


class imageGauche800X350Admin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('file', 'file', array(
                'required' => false
            ))
        ;
    }

    public function prePersist($image)
    {
        $this->manageFileUpload($image);
    }

    public function preUpdate($image)
    {
        $this->manageFileUpload($image);
    }

    private function manageFileUpload($image)
    {
        if ($image->getFile()) {
            $image->refreshUpdated();
        }
    }

    // ...
}
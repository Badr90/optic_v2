<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints as Assert;

use Symfony\Component\HttpFoundation\File\File;
/**
 * Article
 *
 * @ORM\Table(name="article")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ArticleRepository")
 */
class Article
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\Column(name="articleUrl", type="string", nullable=true)
     */
    private $articleUrl;

    /**
     *
     * @ORM\Column(name="details", type="string", nullable=false)
     */
    private $details;



    /**
     *
     * @ORM\Column(name="imageDroite600X250", type="blob", nullable=true)
     */
    private $imageDroite600X250;

    /**
     *
     * @ORM\Column(name="imageDroite600X250path", type="string", nullable=true)
     */
    private $imageDroite600X250path;


    /**
     *
     *
     * @ORM\Column(name="imageDroite800x350", type="blob", nullable=true)
     */
    private $imageDroite800x350;

    /**
     *
     * @ORM\Column(name="imageDroite800x350path", type="string", nullable=true)
     */
    private $imageDroite800x350path;

    /**
     *
     *
     * @ORM\Column(name="imageGauche600X250", type="blob", nullable=false)
     */
    private $imageGauche600X250;

    /**
     *
     * @ORM\Column(name="imageGauche600X250path", type="string", nullable=true)
     */
    private $imageGauche600X250path;

    /**
     *
     *
     * @ORM\Column(name="imageGauhe800x350", type="blob", nullable=false)
     */
    private $imageGauche800x350;

    /**
     *
     * @ORM\Column(name="imageGauche800x350path", type="string", nullable=true)
     */
    private $imageGauche800x350path;

    /**
     **
     * @ORM\Column(name="imageProfil800x350", type="blob", nullable=true)
     */
    private $imageProfil800x350;

    /**
     **
     * @ORM\Column(name="imageProfil800x350path", type="string", nullable=true)
     */
    private $imageProfil800x350path;

    /**
     *
     * @ORM\Column(name="imageProfil600X250", type="blob", nullable=true)
     */
    private $imageProfil600X250;

    /**
     **
     * @ORM\Column(name="imageProfil600X250path", type="string", nullable=true)
     */
    private $imageProfil600X250path;

    /**
     * Article constructor.
     */
    public function __construct(array $config = array())
    {
        $this->setOptions($config);

        $this->dateCreation = new \DateTime();
        }
    /**
     * @return string
     */
    public function getArticleurl()
    {
        return $this->articleUrl;
    }

    /**
     * @return string
     */
    public function getDetails()
    {
        return $this->details;
    }


    /**
     * @return string
     */
    public function getImageDroite600X250()
    {
        return $this->imageDroite600X250;
    }

    /**
     * @param string $imageDroite600X250
     */
    public function setImageDroite600X250($imageDroite600X250)
    {
    	$this->imageDroite600X250 = $imageDroite600X250;
    	if($imageDroite600X250 !=null)
    	{
    		$imageDroite600X250 = file_get_contents($imageDroite600X250);
    		$path='articles\\'.$this->getMarque()->getNomMarque().'\\imageDroite600X250'.
    				bin2hex(random_bytes(16));
    				$this->imageDroite600X250path=$path;
    				file_put_contents(
    						$path ,$imageDroite600X250
    						);
    	}
    }

    /**
     * @return string
     */
    public function getImageDroite800x350()
    {
        return $this->imageDroite800x350;
    }

    /**
     * @param string $imageDroite800x350
     */
    public function setImageDroite800x350($imageDroite800x350)
    {

    	$this->imageDroite800X350 = $imageDroite800x350;
    	if($imageDroite800x350 !=null)
    	{
    		$imageDroite800x350 = file_get_contents($imageDroite800x350);
    		$path='articles\\'.$this->getMarque()->getNomMarque().'\\imageDroite800x350'.
    				bin2hex(random_bytes(16));
    				$this->imageDroite800x350path=$path;
    				file_put_contents(
    						$path ,$imageDroite800x350
    						);
    	}
    }

    /**
     * @return string
     */
    public function getImageGauche600X250()
    {
        return $this->imageGauche600X250;
    }

    /**
     * @param string $imageGauche600X250
     */
    public function setImageGauche600X250(  $imageGauche600X250)
    {
    	$this->imageGauche600X250 = $imageGauche600X250;
        if($imageGauche600X250 !=null)
        {
			$imageGauche600X250 = file_get_contents($imageGauche600X250);
        	$path='articles\\'.$this->getMarque()->getNomMarque().'\\ImageGauche600x250'.
            bin2hex(random_bytes(16));
            $this->imageGauche600X250path=$path;
			file_put_contents(
                $path ,$imageGauche600X250
            );
        }
    }

    /**
     * @return string
     */
    public function getImageGauche800x350()
    {
        return $this->imageGauche800x350;
    }

    /**
     * @param string $imageGauche800x350
     */
    public function setImageGauche800x350( $imageGauche800x350)
    {
    	$this->imageGauche800x350 = $imageGauche800x350;
    	if($imageGauche800x350 !=null)
    	{
    		$imageGauche800x350 = file_get_contents($imageGauche800x350);
    		$path='articles\\'.$this->getMarque()->getNomMarque().'\\imageGauche800x350'.
    				bin2hex(random_bytes(16));
    				$this->imageGauche800x350path=$path;
    				file_put_contents(
    						$path ,$imageGauche800x350
    						);
    	}
    }

    /**
     * @return string
     */
    public function getImageProfil600X250()
    {
        return $this->imageProfil600X250;
    }

    /**
     * @param string $imageProfil600X250
     */
    public function setImageProfil600X250($imageProfil600X250)
    {
    	$this->imageProfil600X250 = $imageProfil600X250;
    	if($imageProfil600X250 !=null)
    	{
    		$imageProfil600X250 = file_get_contents($imageProfil600X250);
    		$path='articles\\'.$this->getMarque()->getNomMarque().'\\imageProfil600X250'.
    				bin2hex(random_bytes(16));
    				$this->imageProfil600X250path=$path;
    				file_put_contents(
    						$path ,$imageProfil600X250
    						);
    	}
    }

    /**
     * @return string
     */
    public function getImageProfil800x350()
    {
        return $this->imageProfil800x350;
    }

    /**
     * @param string $imageProfil800x350
     */
    public function setImageProfil800x350($imageProfil800x350)
    {
    	$this->imageProfil800x350 = $imageProfil800x350;
    	if($imageProfil800x350 !=null)
    	{
    		$imageProfil800x350 = file_get_contents($imageProfil800x350);
    		$path='articles\\'.$this->getMarque()->getNomMarque().'\\imageProfil800x350'.
    				bin2hex(random_bytes(16));
    				$this->imageProfil800x350path=$path;
    				file_put_contents(
    						$path ,$imageProfil800x350
    						);
    	}
    }

    /**
     * @return string
     */
    public function getTypeLunettes()
    {
        return $this->typeLunettes;
    }

    /**
     * @param string $typeLunettes
     */
    public function setTypeLunettes($typeLunettes)
    {
        $this->typeLunettes = $typeLunettes;
    }

    /**
     * @param string $articleurl
     */
    public function setArticleurl($articleurl)
    {
        $this->articleUrl = str_replace(' ', '',$this->getDescription());
    }

    /**
     * @param string $articleurl
     */
    public function setDetails($details)
    {
        $this->details = $details;
    }


    /**
     * @var string
     *
     * @ORM\Column(name="typeLunettes", type="string", length=255)
     */
    private $typeLunettes;

    /**
     * @var bool
     *
     * @ORM\Column(name="enStock", type="boolean")
     */
    private $enStock;

    /**
     * @var bool
     *
     * @ORM\Column(name="prix", type="integer", nullable=true)
     */
    private $prix;

    /**
     * @var bool
     *
     * @ORM\Column(name="code_article", type="integer", nullable=true)
     */
    private $codeArticle;

    /**
     * @var string
     * @Assert\Regex("[^\S*$]",
     *  message="Priere de supprimer les espaces de la référence svp (attacher les caractères) ")
     * @ORM\Column(name="description", type="string", length=2048)
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="nombre_visites", type="integer")
     */
    private $nombreVisites =0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Date_creation", type="datetime")
     */
    private $dateCreation;



    /**
     * @ORM\ManyToOne(targetEntity="Marque", inversedBy="articles")
     *
     */
    private $marque;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Get image
     *
     * @return \AppBundle\Entity\Image
     */
    public function getEncodedDroiteImage600x250()
    {

        $res=base64_encode(stream_get_contents( $this->getImageDroite600X250()) );
        return $res;
    }

    /**
     * Get image
     *
     * @return \AppBundle\Entity\Image
     */
    public function getEncodedDroiteImage800x350()
    {

        $res=base64_encode(stream_get_contents( $this->getImageDroite800x350()) );
        return $res;
    }

    /**
     * Get image
     *
     * @return \AppBundle\Entity\Image
     */
    public function getEncodedGaucheImage600x250()
    {
        $res=base64_encode(stream_get_contents( $this->getImageGauche600X250()) );
        return $res;
    }

    /**
     * Get image
     *
     * @return \AppBundle\Entity\Image
     */
    public function getEncodedGaucheImage800x350()
    {

        $res=base64_encode(stream_get_contents( $this->getImageGauche800x350()) );
        return $res;
    }





    /**
     * Get image
     *
     * @return \AppBundle\Entity\Image
     */
    public function getEncodedProfilImage600x250()
    {

        $res=base64_encode(stream_get_contents( $this->getImageProfil600X250()) );
        return $res;
    }

    /**
     * Get image
     *
     * @return \AppBundle\Entity\Image
     */
    public function getEncodedProfilImage800x350()
    {

        $res=base64_encode(stream_get_contents( $this->getImageProfil800x350()) );
        return $res;
    }




   /**
     * Set type
     *
     * @param string $type
     *
     * @return Article
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set enStock
     *
     * @param boolean $enStock
     *
     * @return Article
     */
    public function setEnStock($enStock)
    {
        $this->enStock = $enStock;

        return $this;
    }

    /**
     * Get enStock
     *
     * @return bool
     */
    public function getEnStock()
    {
        return $this->enStock;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Article
     */
    public function setDescription($description)
    {
        $this->description = $description;
        $this->articleUrl = str_replace(' ', '',$description);

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set nombreVisites
     *
     * @param integer $nombreVisites
     *
     * @return Article
     */
    public function setNombreVisites($nombreVisites)
    {
        $this->nombreVisites = $nombreVisites;

        return $this;
    }

    /**
     * Get nombreVisites
     *
     * @return int
     */
    public function getNombreVisites()
    {
        return $this->nombreVisites;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     *
     * @return Article
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set marque
     *
     * @param \AppBundle\Entity\Marque $marque
     * @return Article
     */
    public function setMarque(\AppBundle\Entity\Marque $marque = null)
    {
        $this->marque = $marque;

        return $this;
    }

    /**
     * Get marque
     *
     * @return \AppBundle\Entity\Marque
     */
    public function getMarque()
    {
        return $this->marque;
    }

    public function setOptions(array $options)
    {
        $_classMethods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $_classMethods)) {
                $this->$method($value);
            } else {
                throw new Exception('Invalid method name');
            }
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImageDroite600X250path()
    {
        return $this->imageDroite600X250path;
    }

    /**
     * @param mixed $imageDroite600X250path
     */
    public function setImageDroite600X250path($imageDroite600X250path)
    {
       // $this->imageDroite600X250path = $imageDroite600X250path;
    }

    /**
     * @return mixed
     */
    public function getImageDroite800x350path()
    {
        return $this->imageDroite800x350path;
    }

    /**
     * @param mixed $imageDroite800x350path
     */
    public function setImageDroite800x350path($imageDroite800x350path)
    {
        //$this->imageDroite800x350path = $imageDroite800x350path;
    }

    /**
     * @return mixed
     */
    public function getImageGauche600X250path()
    {
        return $this->imageGauche600X250path;
    }

    /**
     * @param mixed $imageGauche600X250path
     */
    public function setImageGauche600X250path($imageGauche600X250path)
    {
      //  $this->imageGauche600X250path = $imageGauche600X250path;
     
        
    }

    /**
     * @return mixed
     */
    public function getImageGauche800x350path()
    {
        return $this->imageGauche800x350path;
    }

    /**
     * @param mixed $imageGauche800x350path
     */
    public function setImageGauche800x350path($imageGauche800x350path)
    {
       // $this->imageGauche800x350path = $imageGauche800x350path;
    }

    /**
     * @return mixed
     */
    public function getImageProfil800x350path()
    {
        return $this->imageProfil800x350path;
    }

    /**
     * @param mixed $imageProfil800x350path
     */
    public function setImageProfil800x350path($imageProfil800x350path)
    {
        //$this->imageProfil800x350path = $imageProfil800x350path;
    }

    /**
     * @return mixed
     */
    public function getImageProfil600X250path()
    {
        return $this->imageProfil600X250path;
    }

    /**
     * @param mixed $imageProfil600X250path
     */
    public function setImageProfil600X250path($imageProfil600X250path)
    {
       // $this->imageProfil600X250path = $imageProfil600X250path;
    }

}


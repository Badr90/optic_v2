<?php

namespace TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Image
 *
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks
 */
class Image //extends BaseEntity
{

	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * Image path
	 *
	 * @var string
	 *
	 * @ORM\Column(type="text", length=255, nullable=false)
	 */
	protected $path;

	/**
	 * Image file
	 *
	 * @var File
	 *
	 * @Assert\File(
	 *     maxSize = "5M",
	 *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
	 *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
	 *     mimeTypesMessage = "Only the filetypes image are allowed."
	 * )
	 */
	protected $file;


	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}


	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getFile()
	{
		return $this->file;
	}

	/**
	 * Set file
	 *
	 * @param string $file
	 *
	 * @return Image
	 */
	public function setFile($file)
	{
		$this->file = $file;

		return $this;
	}



	/**
	 * Get path
	 *
	 * @return string
	 */
	public function getPath()
	{
		return $this->path;
	}

	/**
	 * Set path
	 *
	 * @param string $path
	 *
	 * @return Image
	 */
	public function setPath($path)
	{
		$this->path = $path;

		return $this;
	}


	/**
	 * Called before saving the entity
	 *
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 */
	public function preUpload()
	{
		if (null !== $this->file) {
			// do whatever you want to generate a unique name
			$filename = sha1(uniqid(mt_rand(), true));
			$this->path = $filename.'.'.$this->file->guessExtension();
		}
	}


	/**
	 * Called before entity removal
	 *
	 * @ORM\PreRemove()
	 */
	public function removeUpload()
	{
		if ($file = $this->getAbsolutePath()) {
			unlink($file);
		}
	}


	//This is the important part where your file is actually moved to the right directory. Note that I have used some additional methods

	/**
	 * Called after entity persistence
	 *
	 * @ORM\PostPersist()
	 * @ORM\PostUpdate()
	 */
	public function upload()
	{
		// The file property can be empty if the field is not required
		if (null === $this->file) {
			return;
		}

		// Use the original file name here but you should
		// sanitize it at least to avoid any security issues

		// move takes the target directory and then the
		// target filename to move to
		$this->file->move(
				$this->getUploadRootDir(),
				$this->path
				);

		// Set the path property to the filename where you've saved the file
		//$this->path = $this->file->getClientOriginalName();

		// Clean up the file property as you won't need it anymore
		$this->file = null;
	}


}